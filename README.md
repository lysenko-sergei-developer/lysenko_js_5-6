# lysenko_js_5-6 - Time
This is project with homework #5-6 javascript for goit club
## TODO LIST
* Script - Timer
 * LeftSide - StopWatch
   * StopWatch - OnClick()
     * Slide - SlideToStopWatch()
       * ScoreBoard - ScoreBoard.Object()
       * StartButton - StartStopWatch()
         * PauseButton - PauseStopWatch()
         * ContinueButton - ContinueStopWatch()
         * ClearButton - ClearStopWatch()
       * Slide - BackToMain()
 * RightSide - CountDown
    * CountDown - OnClick()
      * Slide - SlideCountDown()
        * ScoreBoard - ScoreBoard.Object()
        * Number - InputNumber()
        * SetButton - SetCountDown()
          * StartButton - StartCountDown()
          * ClearButton - ClearCountDown()
          * Slide - BackToMain()
        * ClearButton - ClearCountDown()
        * Slide - BackToMain()
