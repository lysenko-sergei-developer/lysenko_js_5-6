function callStopWatch() {
  let stopWatch = document.getElementsByClassName('stop__watch')[0];
  let stopWatchContainer = document.getElementsByClassName('stop__watch__container')[0];

  stopWatch.onclick = function(e) {
    var e = e || window.event;
    var target = e.target || e.srcElement;
    if (this == target) {
      stopWatchContainer.style.bottom = '0px';
      stopWatchContainer.style.transition = '1s';

      callBackToMain(stopWatchContainer);
      startStopWatch();
      pauseStopWatch();
      continueStopWatch();
      clearStopWatch();
    }
  }
}

function callCountDown() {
  let countDown = document.getElementsByClassName('count__down')[0];
  let countDownContainer = document.getElementsByClassName('count__down__container')[0];

  countDown.onclick = function(e) {
    var e = e || window.event;
    var target = e.target || e.srcElement;
    if (this == target) {
      countDownContainer.style.top = '0px';
      countDownContainer.style.transition = '1s';

      callBackToMain(countDownContainer);
      inputTime();
      setCountDown();
    }
  }
}

function callBackToMain(currentContainer) {

  let container = currentContainer;
  let back = container.getElementsByClassName('back__to__main')[0];

  back.onclick = function(e) {

    if (container.className == 'stop__watch__container') {
      container.style.bottom = '514px';
      destructStopWatch();

      if (newTime != null)
        newTime.clear();

    } else if (container.className == 'count__down__container') {
      container.style.top = '586px';
    }

    container.style.transition = '1s';
  }
}

function startStopWatch() {
  let startButton = document.getElementsByClassName('start__button')[0];

  startButton.onclick = function(e) {
    //TODO: ADD FUNCTIONAL
    newTime = new Time("stopWatch");
    interval = setInterval(newTime.start, 10);
    startButton.style.zIndex = '1';
  }
}

function pauseStopWatch() {
  let pauseButton = document.getElementsByClassName('pause__button')[0];

  pauseButton.onclick = function(e) {
    //TODO: ADD FUNCTIONAL
    newTime.resume();
    pauseButton.style.zIndex = '1';
  }
}

function continueStopWatch() {
  let continueButton = document.getElementsByClassName('continue__button')[0];
  let pauseButton = document.getElementsByClassName('pause__button')[0];

  continueButton.onclick = function(e) {
    //TODO: ADD FUNCTIONAL
    newTime.resume();
    continueButton.style.zIndex = '1';
    pauseButton.style.zIndex = '2';
  }
}

function clearStopWatch() {
  let clearButton = document.getElementsByClassName('clear__button')[0];

  clearButton.onclick = function(e) {
    //TODO: ADD FUNCTIONAL
    newTime.clear();
    destructStopWatch();
  }
}

function destructStopWatch() {
  let startButton = document.getElementsByClassName('start__button')[0];
  let pauseButton = document.getElementsByClassName('pause__button')[0];
  let continueButton = document.getElementsByClassName('continue__button')[0];
  let scoreboardTime = document.getElementsByClassName('scoreboard__time')[0];
  let scoreboardMSec = document.getElementsByClassName('scoreboard__msec')[0];


  startButton.style.zIndex = '4';
  pauseButton.style.zIndex = '3';
  continueButton.style.zIndex = '2';
  scoreboardTime.innerHTML = '0:00:00';
  scoreboardMSec.innerHTML = '0';
}

Time = function(newTypeOfSign) {
  var time = {
    msec: 0,
    sec: 0,
    minute: 0,
    hour: 0,
  };
  var resumeTime = false;
  var typeOfSign = newTypeOfSign; // type of stopWatch or countDown
  var freezeTick = 0;
  var tick = 0;

  this.start = function() {
    typeOfSign == 'stopWatch' ? tick++ : tick -= 10;

    time.msec = Math.floor(tick * 10 % 1000);
    time.sec = Math.floor(tick / 100 % 60);
    time.minute = Math.floor(tick / 100 / 60 % 60);
    time.hour = Math.floor(tick / 100 / 60 / 60);

    if (time.minute < 10 && typeOfSign == 'stopWatch') time.minute = '0' + time.minute;
    if (time.sec < 10 && typeOfSign == 'stopWatch') time.sec = '0' + time.sec;

    var scoreboard = time.hour + ':' + time.minute + ':' + time.sec;
    var scoreboardMSec = time.msec;

    typeOfSign == 'stopWatch' ? stopWatchTime.innerHTML = scoreboard:
                                countDownTime.innerHTML = scoreboard;

    console.log(tick + "    " + time.sec);

    stopWatchTimeMSec.innerHTML = scoreboardMSec;
  };

  this.toWatchTime = function() {
    countDownString = countDownString.split(':');

    for (var i = 0; i < countDownString.length; i++) {
      // console.log(countDownString[i] + ' # ' + i );
      let curSymbol = countDownString[i];

      curSymbol *= 100;

      if (i == 1)
        curSymbol *= 60;

      if (i == 2)
        curSymbol *= 3600;

      tick = curSymbol;
      // console.log(curSymbol);
    }
  };

  this.resume = function() {

    if (resumeTime == true) {
      resumeTime = false;
      tick = freezeTick;
      interval = setInterval(newTime.start, 100);
    } else {
      resumeTime = true;
      freezeTick = tick;
      clearInterval(interval);
    }
  };

  this.clear = function() {
    clearInterval(interval);
    newTime = null;
  };
};

function inputTime() {
  let ArrayOfNumber = [] ;

  for (var i = 0; i < 10; i++) {
    ArrayOfNumber[i] = document.getElementsByClassName('number')[i];
  }

  ArrayOfNumber[0].onclick = function() {
    currentNumber = 0;

    redactionString(currentNumber);
  }

  ArrayOfNumber[1].onclick = function() {
    currentNumber = 1;

    redactionString(currentNumber);
  }

  ArrayOfNumber[2].onclick = function() {
    currentNumber = 2;

    redactionString(currentNumber);
  }

  ArrayOfNumber[3].onclick = function() {
    currentNumber = 3;

    redactionString(currentNumber);
  }

  ArrayOfNumber[4].onclick = function() {
    currentNumber = 4;

    redactionString(currentNumber);
  }

  ArrayOfNumber[5].onclick = function() {
    currentNumber = 5;

    redactionString(currentNumber);
  }

  ArrayOfNumber[6].onclick = function() {
    currentNumber = 6;

    redactionString(currentNumber);
  }

  ArrayOfNumber[7].onclick = function() {
    currentNumber = 7;

    redactionString(currentNumber);
  }

  ArrayOfNumber[8].onclick = function() {
    currentNumber = 8;

    redactionString(currentNumber);
  }

  ArrayOfNumber[9].onclick = function() {
    currentNumber = 9;

    redactionString(currentNumber);
  }
}

function redactionString(currentNumber) {


  countDownString += currentNumber.toString();
  countDownTime.innerHTML = countDownString;

  if ( ((countDownString.length == 2) || (countDownString.length == 5)) && (countDownString.length < 7) ) {
    countDownString = countDownString + ':';
  } else if (countDownString.length > 7) {
    countDownString = '';
  }

}

function setCountDown() {
  buttonSet = document.getElementsByClassName('set__count__down')[0];

  buttonSet.onclick = function() {
    newTime = new Time('countDown');
    //TODO: FIX
    newTime.toWatchTime();
    console.log(newTime.tick);
    // interval = setInterval(newTime.start, 100);
  }
}

var stopWatchTime = document.getElementsByClassName("scoreboard__time")[0];
var stopWatchTimeMSec = document.getElementsByClassName("scoreboard__msec")[0];

var countDownTime = document.getElementsByClassName("scoreboard__time")[1];
var countDownTimeMSec = document.getElementsByClassName("scoreboard__msec")[1];

var countDownString = ''
var interval;
var newTime;

function initialize() {
  callStopWatch();
  callCountDown();
}

initialize();
